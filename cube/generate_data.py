import cv2
import torch
from keras.datasets import mnist
import numpy
import math
import os

(x_train, y_train), (x_test, y_test) = mnist.load_data()
width=28
height=28
x_train = x_train.astype('float32') / 255.
x_test  = x_test .astype('float32') / 255.


x_test=x_test

W=128
context_N=100

def get_edge_index_and_angle(context_index):


    if context_index>=34:
        return 4,(context_index-34)*5.0
    else:
        return 0,(context_index-16.5)*5.0

def DrawEdge(index,texture,alpha):
    ps=[]
    res_img=numpy.zeros((W,W,1))
    if index==0:
        ps.append(numpy.array([1.0,-1.0,-1.0]))
        ps.append(numpy.array([1.0,1.0,-1.0]))
        ps.append(numpy.array([-1.0,1.0,-1.0]))
        ps.append(numpy.array([-1.0,-1.0,-1.0]))
    if index==1:
        ps.append(numpy.array([-1.0,-1.0,-1.0]))
        ps.append(numpy.array([-1.0,1.0,-1.0]))
        ps.append(numpy.array([-1.0,1.0,1.0]))
        ps.append(numpy.array([-1.0,-1.0,1.0]))
    if index==2:
        ps.append(numpy.array([-1.0,-1.0,1.0]))
        ps.append(numpy.array([-1.0,1.0,1.0]))
        ps.append(numpy.array([1.0,1.0,1.0]))
        ps.append(numpy.array([1.0,-1.0,1.0]))
    if index==3:
        ps.append(numpy.array([1.0,-1.0,1.0]))
        ps.append(numpy.array([1.0,1.0,1.0]))
        ps.append(numpy.array([1.0,1.0,-1.0]))
        ps.append(numpy.array([1.0,-1.0,-1.0]))
    if index==4:
        ps.append(numpy.array([1.0,-1.0,-1.0]))
        ps.append(numpy.array([-1.0,-1.0,-1.0]))
        ps.append(numpy.array([-1.0,-1.0,1.0]))
        ps.append(numpy.array([1.0,-1.0,1.0]))
    if index==5:
        ps.append(numpy.array([1.0,1.0,1.0]))
        ps.append(numpy.array([-1.0,1.0,1.0]))
        ps.append(numpy.array([-1.0,1.0,-1.0]))
        ps.append(numpy.array([1.0,1.0,-1.0]))

    alpha=alpha*math.pi/180.0
    beta=math.pi/4.0

    R1 = numpy.array([[ math.cos(alpha), 0 ,math.sin(alpha)], [ 0, 1 ,0], [ -math.sin(alpha), 0 ,math.cos(alpha)]])
    R2 = numpy.array([[ 1, 0 ,0], [ 0, math.cos(beta) ,-math.sin(beta)], [ 0, math.sin(beta) ,math.cos(beta)]])
    for i in range(4):
        ps[i]=numpy.dot(R1,ps[i])
    for i in range(4):
        ps[i]=numpy.dot(R2,ps[i])

    a=ps[2]-ps[1]
    b=ps[0]-ps[1]
    c=numpy.cross(a,b)
    if c[2]>0:
        if texture==None:
            for i in range(4):
                i1=i
                i2=i+1
                if i2==4:
                    i2=0
                cv2.line(res_img,(int(ps[i1][0]*W*2/8+W/2),int(ps[i1][1]*W*2/8+W/2)),(int(ps[i2][0]*W*2/8+W/2),int(ps[i2][1]*W*2/8+W/2)),(255,255,255))
        else:
            pts_dst = numpy.zeros((4,2))
            for i in range(4):
                pts_dst[i,0]=ps[i][0]*W*2/8+W/2
                pts_dst[i,1]=ps[i][1]*W*2/8+W/2

            pts_src = numpy.array([[texture.shape[1], 0],[texture.shape[1], texture.shape[0]],[0, texture.shape[0]],[0, 0]])


            h,_ = cv2.findHomography(pts_src, pts_dst)
            im_out = cv2.warpPerspective(texture*255.0, h, (res_img.shape[1],res_img.shape[0]))
            res_img[:,:,0]=res_img[:,:,0]+im_out

    return res_img


'''img=DrawEdge(0,x_train[10]*1.0,70.0)
img+=DrawEdge(3,x_train[0]*1.0,70.0)
img+=DrawEdge(4,x_train[5]*1.0,70.0)
cv2.imwrite('70.png',img)'''

sumImg=numpy.zeros((W*10,W*10,1))
for i in range(context_N):
    edge_index,alpha=get_edge_index_and_angle(i)

    img=DrawEdge(edge_index,numpy.ones((28,28)),alpha)
    ix=i%10
    iy=int(i/10)
    sumImg[iy*W:iy*W+W,ix*W:ix*W+W,0]=img[:,:,0]

cv2.imwrite('0.png',sumImg)

for I in range(60000):
    sumImg=numpy.zeros((W*10,W*10,1))
    for i in range(context_N):
        edge_index,alpha=get_edge_index_and_angle(i)

        img=DrawEdge(edge_index,x_train[I],alpha)
        ix=i%10
        iy=int(i/10)
        sumImg[iy*W:iy*W+W,ix*W:ix*W+W,0]=img[:,:,0]

    cv2.imwrite('train/'+str(I)+'.png',sumImg)

#cv2.imshow('1',img)
#cv2.waitKey(0)

