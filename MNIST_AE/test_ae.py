from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import numpy
import random
import os
import model
import cv2

code_length=16


batch_size=16
learning_rate=0.001

test_data=datasets.MNIST('../data', train=False)

test=numpy.zeros((len(test_data),28*28))
test_labels=numpy.zeros((len(test_data)),dtype=numpy.int32)
for i in range(len(test_data)):
    ar=numpy.array(test_data[i][0])/255.0
    ar=ar.reshape((28*28))
    test[i,:]=ar[:]
    test_labels[i]=test_data[i][1]

#########INIT AUTOENCODER##########
model.init(True)

n_classes=model.n_classes



for i in range(100):
    label=test_labels[i]
    batch_xs=numpy.zeros((1,28*28))
    batch_xs[0,:]=test[i,:]
    img = Variable(torch.from_numpy(batch_xs).float(), requires_grad=False).cuda()
    output,code=model.models[label](img)
    temp=output[0].data.cpu().numpy()
    cv2.imwrite('test/'+str(i)+'.png',temp.reshape((28,28))*255.0)
    output=output

