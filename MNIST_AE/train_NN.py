from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import os
import numpy
import random


########## VARIABLES ##########
n_classes=10
latent_dim=16
learning_rate=0.001
models=[]
criterions=[]
optimizers=[]
batch_size=128

###############################

class NN(nn.Module):

    def __init__(self):
        super(NN, self).__init__()

        self.layer1= nn.Linear(28 * 28, 2048)
        self.layer2=nn.Linear(2048,10)

    def forward(self, x):
        x =functional.relu(self.layer1(x))
        res=functional.softmax(self.layer2(x))

        return res




#######LOAD AND PREPAIR DATA######
train_data=datasets.MNIST('../data',train=True, download=True)
test_data=datasets.MNIST('../data', train=False)
train=numpy.zeros((len(train_data),28*28))
train_labels=numpy.zeros((len(train_data)),dtype=numpy.int32)
for i in range(len(train_data)):
    ar=numpy.array(train_data[i][0])/255.0
    ar=ar.reshape((28*28))
    train[i,:]=ar[:]
    train_labels[i]=train_data[i][1]
test=numpy.zeros((len(test_data),28*28))
test_labels=numpy.zeros((len(train_data)),dtype=numpy.int32)
for i in range(len(test_data)):
    ar=numpy.array(test_data[i][0])/255.0
    ar=ar.reshape((28*28))
    test[i,:]=ar[:]
    test_labels[i]=test_data[i][1]


##################
### INIT
##################
m = NN().cuda()
criterion = nn.BCELoss()
optimizer = torch.optim.Adam( m.parameters(), lr=0.001, weight_decay=1e-5)

for i in range(50000):
    batch_xs=numpy.zeros((batch_size,28*28))
    batch_ys=numpy.zeros((batch_size,10))
    #batch_noise=numpy.random.normal(scale=gauss,size=(batch_size,n_latent))
    for j in range(batch_size):

        index=random.randint(0,len(train_data)-1)
        batch_xs[j,:] = train[index][:]
        batch_ys[j,train_labels[index]]=1


    x = Variable(torch.from_numpy(batch_xs).float(), requires_grad=False).cuda()
    output=m(x)

    y = Variable(torch.from_numpy(batch_ys).float(), requires_grad=False).cuda()
    loss=criterion(output,y)#+0.001*criterion(dev,one)+0.001*criterion(zero,one)
    #losses.append(loss)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if i%100==0:
        print('epoch [{}], loss:{:.4f}'.format(i, loss.data[0]))
    if i%1000==0:
        x = Variable(torch.from_numpy(test).float(), requires_grad=False).cuda()
        output=m(x)
        succ=0
        N=test.shape[0]
        for j in range(test.shape[0]):
            if output.data.cpu()[j].numpy().argmax()==test_labels[j]:
                succ+=1
        print('accuracy',succ/N)