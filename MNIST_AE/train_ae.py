from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import numpy
import random
import os
import model
import cv2


batch_size=16
learning_rate=0.001


#######LOAD AND PREPAIR DATA######
train_data=datasets.MNIST('../data',train=True, download=True)
test_data=datasets.MNIST('../data', train=False)
train=numpy.zeros((len(train_data),28*28))
train_labels=numpy.zeros((len(train_data)),dtype=numpy.int32)
for i in range(len(train_data)):
    ar=numpy.array(train_data[i][0])/255.0
    ar=ar.reshape((28*28))
    train[i,:]=ar[:]
    train_labels[i]=train_data[i][1]
test=numpy.zeros((len(test_data),28*28))
test_labels=numpy.zeros((len(train_data)),dtype=numpy.int32)
for i in range(len(test_data)):
    ar=numpy.array(test_data[i][0])/255.0
    ar=ar.reshape((28*28))
    test[i,:]=ar[:]
    test_labels[i]=test_data[i][1]


###########INIT #########
model.init(False)


n_classes=model.n_classes
n_rounds=50000

for i in range(n_rounds):
    losses=[]
    for t in range(n_classes):
        batch_xs=numpy.zeros((batch_size,28*28))
        batch_ys=numpy.zeros((batch_size,28*28))
        #batch_noise=numpy.random.normal(scale=gauss,size=(batch_size,n_latent))
        for j in range(batch_size):
            index=0
            while True:
                index=random.randint(0,len(train_data)-1)
                if train_labels[index]==t:
                    batch_xs[j,:] = train[index][:]
                    batch_ys[j,:] = train[index][:]
                    break
        loss=model.train_signle_AE(t, batch_xs)
        losses.append(loss)



    if i%100==0:
        for t in range(n_classes):
            print('epoch [{}], loss:{:.4f}'.format(i, losses[t].data[0]))
        print('---------')
    if i%1000==0:
        model.save()