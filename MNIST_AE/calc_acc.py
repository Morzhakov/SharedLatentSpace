from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import numpy
import random
import os
import model
import cv2

test_codes=numpy.load('data/test_codes.npy')
test_labels=numpy.load('data/test_labels.npy')
test_dists=numpy.load('data/test_dists.npy')
train_codes=numpy.load('data/train_codes.npy')
train_labels=numpy.load('data/train_labels.npy')
train_dists=numpy.load('data/train_dists.npy')

model.init(True)

for t in range(10):
    img=Variable(torch.from_numpy(test_codes[t,0,:]).float(), requires_grad=False).cuda()
    output=model.models[t].decode(img)
    res=output.data.cpu().numpy()
    cv2.imwrite('temp/'+str(t)+'.png',res.reshape((28,28))*255.0)


succ=0
N=0
K=5

for i in range(test_dists.shape[1]):
    dists=test_dists[:,i]
    index=dists.argmin()
    if index==test_labels[i]:
        succ+=1
    N+=1

print(succ/N)