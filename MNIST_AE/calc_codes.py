from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import numpy
import random
import os
import model
import cv2



#######LOAD AND PREPAIR DATA######
train_data=datasets.MNIST('../data',train=True, download=True)
test_data=datasets.MNIST('../data', train=False)
train=numpy.zeros((len(train_data),28*28))
train_labels=numpy.zeros((len(train_data)),dtype=numpy.int32)
for i in range(len(train_data)):
    ar=numpy.array(train_data[i][0])/255.0
    ar=ar.reshape((28*28))
    train[i,:]=ar[:]
    train_labels[i]=train_data[i][1]
test=numpy.zeros((len(test_data),28*28))
test_labels=numpy.zeros((len(train_data)),dtype=numpy.int32)
for i in range(len(test_data)):
    ar=numpy.array(test_data[i][0])/255.0
    ar=ar.reshape((28*28))
    test[i,:]=ar[:]
    test_labels[i]=test_data[i][1]


###########INIT #########
model.init(True)
n_classes=model.n_classes

test_codes=numpy.zeros((n_classes, test.shape[0], model.latent_dim))
test_dists=numpy.zeros((n_classes,test.shape[0]))
train_codes=numpy.zeros((n_classes, train.shape[0], model.latent_dim))
train_dists=numpy.zeros((n_classes,train.shape[0]))

detWs=numpy.zeros((n_classes,test.shape[0]))
for t in range(n_classes):
    m=model.models[t]
    #train
    img = Variable(torch.from_numpy(train).float(), requires_grad=False).cuda()
    output,code=m(img)
    res=code.data.cpu().numpy()
    train_codes[t,:,:]=res[:,:]
    res=output.data.cpu().numpy()
    for i in range(res.shape[0]):
        d=numpy.linalg.norm(res[i]-train[i])
        train_dists[t,i]=d

    #test
    img = Variable(torch.from_numpy(test).float().cuda(), requires_grad=True)
    W=numpy.zeros((test.shape[0],32,784))
    output,code=m(img)



    res=code.data.cpu().numpy()
    test_codes[t,:,:]=res[:,:]
    res=output.data.cpu().numpy()
    for i in range(res.shape[0]):
        d=numpy.linalg.norm(res[i]-test[i])
        test_dists[t,i]=d

    #grads
    '''for k in range(model.latent_dim):
        if not img.grad is None:
            img.grad.data.zero_()
        output,code=m(img)
        temp=numpy.zeros((test.shape[0],model.latent_dim))
        temp[:,k]=1.0
        code.backward( Variable(torch.from_numpy(temp).float().cuda(),requires_grad=False))
        grad=img.grad.data.cpu().numpy()
        W[:,k,:]=grad[:,:]
    for i in range(test.shape[0]):
        W2=numpy.dot(W[i,:,:],numpy.transpose(W[i,:,:]))
        W2=W2+numpy.identity(32)*0.1
        detW=numpy.linalg.det(W2)
        detWs[t,i]=detW
        eigs=numpy.linalg.eig(W2)
        detW=detW
    detWs=detWs'''

if not os.path.isdir('data'):
    os.mkdir('data')
numpy.save('data/train_codes',train_codes)
numpy.save('data/train_dists',train_dists)
numpy.save('data/train_labels',train_labels)
numpy.save('data/test_codes',test_codes)
numpy.save('data/test_dists',test_dists)
numpy.save('data/test_labels',test_labels)
