from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import os


########## VARIABLES ##########
n_classes=10
latent_dim=16
learning_rate=0.001
models=[]
criterion=None
optimizer=None

###############################

class autoencoder(nn.Module):
    '''def __init__(self):
        super(autoencoder, self).__init__()
        h_layers=256

        self.layer1= nn.Linear(28 * 28, h_layers)
        self.layer2= nn.Linear(h_layers,h_layers)
        self.layer3=nn.Linear(h_layers,latent_dim)
        self.bias3=  nn.Parameter(torch.randn(h_layers))
        self.bias2=  nn.Parameter( torch.randn(h_layers))
        self.bias1=   nn.Parameter(torch.randn(28*28))

    def forward(self, x):
        x =functional.relu(self.layer1(x))
        x= functional.relu(self.layer2(x))
        code=functional.sigmoid(self.layer3(x))
        #code=self.layer3(x)
        #dev=code.std()
        #mean=code.mean()
        x=functional.relu(functional.linear(input=code,weight=self.layer3.weight.t(),bias=self.bias3))
        x=functional.relu(functional.linear(input=x,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.sigmoid(functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1))
        return x,code#,dev,mean
    def decode(self,code):
        x=functional.relu(functional.linear(input=code,weight=self.layer3.weight.t(),bias=self.bias3))
        x=functional.relu(functional.linear(input=x,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.sigmoid(functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1))
        return x'''
    def __init__(self):
        super(autoencoder, self).__init__()

        self.layer1= nn.Linear(28 * 28, 2048)
        self.layer2=nn.Linear(2048,latent_dim)
        self.bias2=  nn.Parameter(torch.randn(2048))
        self.bias1=   nn.Parameter(torch.randn(28*28))

    def forward(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=self.layer2(x)
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.sigmoid(functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1))
        return x,code
    def encode(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=self.layer2(x)
        return code
    def decode(self,code):
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.sigmoid(functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1))
        return x

####################
### FUNTIONS
####################

def train_signle_AE(index,batch):
    ae=models[index]

    img = Variable(torch.from_numpy(batch).float(), requires_grad=False).cuda()
    output,code=ae(img)

    zero = Variable(torch.zeros((1)).float(), requires_grad=False).cuda()
    one = Variable(torch.ones((1)).float(), requires_grad=False).cuda()

    loss=criterion(output,img)

    optimizers[index].zero_grad()
    loss.backward()
    optimizers[index].step()
    return loss

def cross_train(index1,batch1,index2,batch2):
    ae1=models[index1]
    ae2=models[index2]

    img1 = Variable(torch.from_numpy(batch1).float(), requires_grad=False).cuda()
    img2 = Variable(torch.from_numpy(batch2).float(), requires_grad=False).cuda()
    code=ae1.encode(img1)
    res=ae2.decode(code)

    loss=criterion(res,img2)#+0.001*criterion(dev,one)+0.001*criterion(zero,one)
    #losses.append(loss)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    return loss

def save():
    if not os.path.isdir('models'):
        os.mkdir('models')
    for t in range(n_classes):
        torch.save(models[t].state_dict(), 'models/ae_'+str(t)+'.pth')
    return

optimizers=[]
##################
### INIT
##################
def init(load_if_exist):
    global criterion
    global optimizers

    criterion = nn.MSELoss()
    params=[]
    for i in range(n_classes):
        m = autoencoder().cuda()
        models.append(m)


        optimizer = torch.optim.Adam( m.parameters(), lr=learning_rate, weight_decay=1e-5)
        optimizers.append(optimizer)
    if load_if_exist:
        if os.path.exists('models/ae_0.pth'):
            for t in range(n_classes):
                models[t].load_state_dict(torch.load('models/ae_'+str(t)+'.pth'))