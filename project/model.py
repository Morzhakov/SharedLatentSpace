from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import os
import random
import numpy

########## VARIABLES ##########
n_contexts=100
latent_dim=16
learning_rate=0.001
models=[]
criterion=None
optimizer=None
input_dim=64*64
hidden_layer=256

###############################

class autoencoder(nn.Module):

    def __init__(self):
        super(autoencoder, self).__init__()

        self.layer1= nn.Linear(input_dim, hidden_layer)
        self.layer2=nn.Linear(hidden_layer,latent_dim)
        self.bias2=  nn.Parameter(torch.randn(hidden_layer))
        self.bias1=   nn.Parameter(torch.randn(input_dim))

    def forward(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=self.layer2(x)
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1)
        return x,code
    def encode(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=self.layer2(x)
        return code
    def decode(self,code):
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1)
        return x

####################
### FUNTIONS
####################

def train(samples):
    outputs=[]
    ys=[]
    for i in range(n_contexts):
        img = Variable(torch.from_numpy(samples[i]).float(), requires_grad=False).cuda()
        output,code=models[i].forward(img)
        outputs.append(output)
        ys.append(img)

    for i in range(int(n_contexts)):
        j1=random.randint(0,n_contexts-1)
        j2=random.randint(0,n_contexts-1)
        img1= Variable(torch.from_numpy(samples[j1]).float(), requires_grad=False).cuda()
        img2= Variable(torch.from_numpy(samples[j2]).float(), requires_grad=False).cuda()
        code=models[j1].encode(img1)
        output=models[j2].decode(code)
        outputs.append(output)
        ys.append(img2)

    output_merge=torch.cat(outputs)
    y_merge=torch.cat(ys)
    loss=criterion(output_merge,y_merge)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    return loss

def train_signle_AE(index,batch):
    ae=models[index]

    img = Variable(torch.from_numpy(batch).float(), requires_grad=False).cuda()
    output,code=ae(img)

    zero = Variable(torch.zeros((1)).float(), requires_grad=False).cuda()
    one = Variable(torch.ones((1)).float(), requires_grad=False).cuda()

    loss=criterion(output,img)

    optimizers[index].zero_grad()
    loss.backward()
    optimizers[index].step()

    return loss

def cross_train(index1,batch1,index2,batch2):
    ae1=models[index1]
    ae2=models[index2]

    img1 = Variable(torch.from_numpy(batch1).float(), requires_grad=False).cuda()
    img2 = Variable(torch.from_numpy(batch2).float(), requires_grad=False).cuda()
    code=ae1.encode(img1)
    res=ae2.decode(code)

    loss=criterion(res,img2)#+0.001*criterion(dev,one)+0.001*criterion(zero,one)
    #losses.append(loss)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    return loss

def predict(input,ae_index):
    img = Variable(torch.from_numpy(input).float(), requires_grad=False).cuda()
    res = models[ae_index].forward(img)

    return res[0].data.cpu().numpy(),res[1].data.cpu().numpy()

def decode(code,ae_index):
    img = Variable(torch.from_numpy(code).float(), requires_grad=False).cuda()
    res = models[ae_index].decode(img)

    return res.data.cpu().numpy()
def save(folder):
    if not os.path.isdir('models'):
        os.mkdir('models')
    for t in range(n_contexts):
        torch.save(models[t].state_dict(), folder+'/ae_'+str(t)+'.pth')
    return




##################
### INIT
##################
def init(load_if_exist,folder):
    global criterion
    global optimizer
    global optimizers

    optimizers=[]

    criterion = nn.MSELoss()
    params=list()


    for i in range(n_contexts):

        m = autoencoder().cuda()
        models.append(m)
    for i in range(n_contexts):
        params+=models[i].parameters()
        opt=torch.optim.Adam( models[i].parameters(), lr=learning_rate, weight_decay=1e-5)
        optimizers.append(opt)

    optimizer = torch.optim.Adam( params, lr=learning_rate)#, weight_decay=1e-5)
    #optimizer = torch.optim.SGD( params, lr=learning_rate)


    if load_if_exist:
        if os.path.exists(folder+'/ae_0.pth'):
            for t in range(n_contexts):
                models[t].load_state_dict(torch.load(folder+'/ae_'+str(t)+'.pth'))