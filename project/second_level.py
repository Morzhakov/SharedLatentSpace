from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import os
import random
import numpy

########## VARIABLES ##########
latent_dim=1
learning_rate=0.001
criterion=None
optimizer=None
input_dim=100
hidden_layer=32
batch_size=128

###############################

class autoencoder(nn.Module):

    def __init__(self):
        super(autoencoder, self).__init__()

        self.layer1= nn.Linear(input_dim, hidden_layer)
        self.layer2=nn.Linear(hidden_layer,latent_dim)
        self.bias2=  nn.Parameter(torch.randn(hidden_layer))
        self.bias1=   nn.Parameter(torch.randn(input_dim))

    def forward(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=functional.sigmoid(self.layer2(x))
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1)
        return x,code
    def encode(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=functional.sigmoid(self.layer2(x))
        return code
    def decode(self,code):
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1)
        return x


train_dists=numpy.load('train_dists.npy')
test_dists=numpy.load('test_dists.npy')

##################
### INIT
##################
m = autoencoder().cuda()
criterion = nn.MSELoss()
optimizer = torch.optim.Adam( m.parameters(), lr=0.001, weight_decay=1e-5)

#train AE

for i in range(100000):

    batch_xs=numpy.zeros((batch_size,100))
    for j in range(batch_xs.shape[0]):
        index=random.randint(0,train_dists.shape[0]-1)
        batch_xs[j,:]=train_dists[j,:]

    x = Variable(torch.from_numpy(batch_xs).float(), requires_grad=False).cuda()
    output,code=m(x)

    y = Variable(torch.from_numpy(batch_xs).float(), requires_grad=False).cuda()
    loss=criterion(output,y)
    #losses.append(loss)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if i%100==0:
        print('epoch [{}], loss:{:.4f}'.format(i, loss.data[0]))

    if i%1000==0:
        torch.save(m.state_dict(), 'ae2.pth')