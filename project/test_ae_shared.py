from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import numpy
import random
import os
import model
import cv2


batch_size=16
learning_rate=0.001

width=64
input_size=width*width


def get_subimg(img,context_index):
    ix=context_index%10
    iy=int(context_index/10)
    return img[width*iy:width*iy+width,width*ix:width*ix+width,0]

#######LOAD AND PREPAIR DATA######
n_contexts=model.n_contexts


###########INIT #########
model.init(True,'models')



context_index0=10
img=cv2.imread('E:/AI/cube/test/'+str(context_index0)+'.png')
img=cv2.resize(img,(width,width))[:,:,0]/255.0
batch_xs=numpy.zeros((1,input_size))
batch_xs[0,:]=img.reshape((input_size))
res=model.predict(batch_xs,context_index0)
code=res[1][0]

resImg=numpy.zeros((width*10,width*10))

for i in range(n_contexts):

    res=model.decode(code,i)
    temp=res.reshape((width,width))
    ix=i%10
    iy=int(i/10)
    resImg[iy*64:iy*64+64,ix*64:ix*64+64]=temp[:,:]*255.0

cv2.imwrite('res.png',resImg)

