import cv2
import math
import generate_data
import model
import numpy


texture_v=cv2.imread('V.bmp')/255.0
texture_plus=cv2.imread('plus.bmp')/255.0


imgs_v=[]
imgs_plus=[]
for i in range(100):
    edge_index,alpha=generate_data.get_edge_index_and_angle(i)
    img=generate_data.DrawEdge(edge_index,texture_v[:,:,0],alpha)
    imgs_v.append(img)
    cv2.imwrite('sharing_samples/v/'+str(i)+'.png',img)

    img=generate_data.DrawEdge(edge_index,texture_plus[:,:,0],alpha)
    imgs_plus.append(img)
    cv2.imwrite('sharing_samples/plus/'+str(i)+'.png',img)

#######LOAD AND PREPAIR DATA######
n_contexts=model.n_contexts
###########INIT #########
model.init(True,'models')

#get codes in the context  numb.10

context_index0=10
width=64
input_size=width*width

#V
img=cv2.resize(imgs_v[context_index0],(width,width))/255.0
batch_xs=numpy.zeros((1,input_size))
batch_xs[0,:]=img.reshape((input_size))
res=model.predict(batch_xs,context_index0)
code_v0=res[1][0]


'''big_image=numpy.zeros((10*64,10*64))
for i in range(100):
    res=model.decode(code_v0,i).reshape((64,64))
    ix=i%10
    iy=int(i/10)
    big_image[iy*64:iy*64+64,ix*64:ix*64+64]=res[:,:]*255.0
cv2.imwrite('big.png',big_image)'''

#plus
img=cv2.resize(imgs_plus[context_index0],(width,width))/255.0
batch_xs=numpy.zeros((1,input_size))
batch_xs[0,:]=img.reshape((input_size))
res=model.predict(batch_xs,context_index0)
code_plus0=res[1][0]


#division plane
center_point=(code_plus0+code_v0)/2.0
n=(code_plus0-code_v0)

v_succ=0
plus_succ=0
d1s=[]
d2s=[]

codes_v=[]
codes_plus=[]

for i in range(100):
    #code v
    img=cv2.resize(imgs_v[i],(width,width))/255.0
    batch_xs=numpy.zeros((1,input_size))
    batch_xs[0,:]=img.reshape((input_size))
    res=model.predict(batch_xs,i)
    code_v=res[1][0]
    codes_v.append(code_v)
    #code plus
    img=cv2.resize(imgs_plus[i],(width,width))/255.0
    batch_xs=numpy.zeros((1,input_size))
    batch_xs[0,:]=img.reshape((input_size))
    res=model.predict(batch_xs,i)
    code_plus=res[1][0]
    codes_plus.append(code_plus)

    d1=numpy.dot((code_v-center_point),n)
    d1s.append(d1)
    if d1<0:
        v_succ+=1
    d2=numpy.dot((code_plus-center_point),n)
    d2s.append(d2)
    if d2>0:
        plus_succ+=1

f=open('hist.txt','w+')
hist1=numpy.zeros((50),dtype=numpy.int)
hist2=numpy.zeros((50),dtype=numpy.int)
for i in range(len(d1s)):
    v=int((d1s[i]+50.0)/2.0)
    hist1[v]+=1
    v=int((d2s[i]+50.0)/2.0)
    hist2[v]+=1
for i in range(hist1.shape[0]):
    f.write(str(hist1[i])+' '+str(hist2[i])+'\n')
f.close()
v_succ=v_succ






