from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import os
import random
import numpy
import cv2
import model
import generate_data


########## VARIABLES ##########
latent_dim=1
learning_rate=0.001
criterion=None
optimizer=None
input_dim=100
hidden_layer=32
batch_size=128

###############################

class autoencoder(nn.Module):

    def __init__(self):
        super(autoencoder, self).__init__()

        self.layer1= nn.Linear(input_dim, hidden_layer)
        self.layer2=nn.Linear(hidden_layer,latent_dim)
        self.bias2=  nn.Parameter(torch.randn(hidden_layer))
        self.bias1=   nn.Parameter(torch.randn(input_dim))

    def forward(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=self.layer2(x)
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1)
        return x,code
    def encode(self, x):
        x =functional.relu(self.layer1(x))
        #code=functional.tanh(self.layer2(x))
        code=self.layer2(x)
        return code
    def decode(self,code):
        x=functional.relu(functional.linear(input=code,weight=self.layer2.weight.t(),bias=self.bias2))
        x=functional.linear(input=x,weight=self.layer1.weight.t(),bias=self.bias1)
        return x


#init second level AE
m=autoencoder().cuda()
m.load_state_dict(torch.load('ae2.pth'))

#init a set of AEs (1st level AEs)
model.init(True,'models')
index_0=14
img_3=cv2.imread('E:/AI/cube/train/135.png')
_,code_3=model.predict(cv2.resize(img_3[int(index_0/10)*128:int(index_0/10)*128+128,(index_0%10)*128:(index_0%10)*128+128,0],(64,64)).reshape(64*64)/255.0,index_0)
img_V=cv2.imread('V.bmp')
edge_index,angle=generate_data.get_edge_index_and_angle(index_0)
res=generate_data.DrawEdge(edge_index,img_V[:,:,0]/255.0,angle)
res=cv2.resize(res,(64,64))
cv2.imwrite('temp.png',res)
_,code_V=model.predict(res.reshape(64*64)/255.0,index_0)



x=numpy.zeros((10,1))
for i in range(10):
    x[i,0]=float(i)/10.0

img = Variable(torch.from_numpy(x).float(), requires_grad=False).cuda()
res=m.decode(img)
res=res.data.cpu().numpy()

for i in range(10):
    img=res[i].reshape((10,10))*100.0
    cv2.imwrite('level2/'+str(i)+'.png',cv2.resize(img,(300,300),interpolation=cv2.INTER_NEAREST))

    #first edge local min
    index1=res[i][0:17].argmin()
    #second edge local min
    index2=res[i][17:34].argmin()+17
    #third edge local min
    index3=res[i][34:100].argmin()+34

    img=numpy.zeros((128,128))
    img+=cv2.resize(model.decode(code_3,index1).reshape((64,64)),(128,128))
    img+=cv2.resize(model.decode(code_3,index2).reshape((64,64)),(128,128))
    img+=cv2.resize(model.decode(code_3,index3).reshape((64,64)),(128,128))
    cv2.imwrite('back_projection/'+str(i)+'.png',img*255.0)

    #get the latent code for the digit