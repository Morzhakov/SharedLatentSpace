from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.nn import functional
import numpy
import random
import os
import model
import cv2




batch_size=16
learning_rate=0.001

width=64
input_size=width*width

N=60000

def get_subimg(img,context_index):
    ix=context_index%10
    iy=int(context_index/10)
    return img[width*iy:width*iy+width,width*ix:width*ix+width,0]
n_contexts=model.n_contexts

def test_models():
    context_index0=10
    img=cv2.imread('E:/AI/cube/test/'+str(context_index0)+'.png')
    img=cv2.resize(img,(width,width))[:,:,0]/255.0
    batch_xs=numpy.zeros((1,input_size))
    batch_xs[0,:]=img.reshape((input_size))
    res=model.predict(batch_xs,context_index0)
    code=res[1][0]

    resImg=numpy.zeros((width*10,width*10))

    for i in range(n_contexts):

        res=model.decode(code,i)
        temp=res.reshape((width,width))
        ix=i%10
        iy=int(i/10)
        resImg[iy*64:iy*64+64,ix*64:ix*64+64]=temp[:,:]*255.0

    cv2.imwrite('res.png',resImg)

###########INIT #########
model.init(False,'models')

n_rounds=150000


M=8000

imgs=[]
for i in range(M):
    index=random.randint(0,N-1)
    img=cv2.imread('E:/AI/cube/train/'+str(index)+'.png')
    img=cv2.resize(img,(width*10,width*10),interpolation=cv2.INTER_NEAREST)
    imgs.append(img)
    if i%100==0:
        print(i)

#cross training
for i in range(n_rounds):
    if i%100==0:
        test_models()
    losses=[]

    #separate training
    samples=[]
    indexes=[]
    for j in range(batch_size):
        index=random.randint(0,M-1)
        indexes.append(index)
    for t in range(n_contexts):
        batch_xs=numpy.zeros((batch_size,input_size))
        #batch_noise=numpy.random.normal(scale=gauss,size=(batch_size,n_latent))
        for j in range(batch_size):

            img=imgs[indexes[j]]

            subimg=get_subimg(img,t)/255.0
            batch_xs[j][:]=subimg.reshape((input_size))
        samples.append(batch_xs)

    loss=model.train(samples)

    if i%100==0:

        print('epoch [{}], loss:{:.8f}'.format(i, loss.item()))
    if i%1000==999:
        model.save('models')
